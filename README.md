## Singing Success Academy Company Website

A website built for IAT339 course challenge

#### Overview

This imaginary company, Singing Success Academy, is an privately established singing institution that helps students succeed in their singing careers. The primary services the company provides are 1-on-1 private lessons that are tailored for every student and their gender, vocal range, and desired singing style.

The website aims to provide prospective clients/students a friendly interface for browsing and choosing the right lessons for them to take. At the same time, it provides existing students and graduates a platform for getting updates and new events they can attend. Due to the limited scope of this project, however, there will be no user authentication and account system implemented for event management and notification.








#### Sections of the website

The website contains 5 sections/pages:

1. Home/Landing: A teaser and call-to-action page for capturing new users’ interests.
2. Lessons: A list of all the lessons currently offered, which has a few sub-pages
  - Lesson details
  - Checkout (info collection)
  - Invoice
3. News & Events: News and event updates of the company, including live social media feed.
4. About: Company’s introduction, history, mission statement, status quo, and testimonials.
5. Contact: Ways to contact the company.







#### Main technology used

- [ReactJS](https://facebook.github.io/react/) for custom and reusable components. When thinking about and breaking down the application into individual components that are isolated, page composition and data flow become easier and more straightforward. For example, when the user’s lesson schedule data (lessons that are purchased and booked) are persisted and retrieved, the lesson schedule component can simply take in the JSON data from the HTTP response and render out the corresponding information. This way, I can treat sections of the app as components where I can just throw new data at them and know exactly how each of them would behave without worrying about which div I need to update when new data comes in.
- [Webpack](https://webpack.github.io/) for module bundling. It helps with concatenating and minifying multiple project files, and at the same time connecting compilers and preprocessors, such as [Babel](https://babeljs.io/) and [Less.js](http://lesscss.org/).
- Custom [Modernizr](https://modernizr.com/) build for detecting browser supports for `flexbox`.
- [Normalize.css](https://necolas.github.io/normalize.css/) for normalizing the default styles across different browsers.
- [Google Fonts](https://www.google.com/fonts) because Lato is optimized for both body texts and larger title texts. It also looks nice for a not-so-serious corporate websites because of its semi-round details of the letters which gives a hint of warmth.

#### Project Structure

The project can be categorized in 4 distinct sections:

1. Project root: Contains mostly the configuration files and this README.
2. `node_modules/`: Where node packages are automatically installed in.
3. `src/`: Where the application source resides. This is where most, if not all, of the development happens in.
4. `build/`: Automatically generated by Webpack that contains all the compiled files, which include:
  - `app.js`: Application Javascript code. The CSS styles will be injected by this file as a style tag in the `head` element. This controversial way of serving CSS as part of the Javascript file theoretically decreases load time by reducing the number of HTTP requests sent on page load. It’s more popular now than ever because of the trend of front-end/Javascript heavy applications. That being said, extracting the CSS into a separate file will be a simple one-line change in Webpack’s configurations.
  - `modernizr.js`: Custom Modernizr build that only checks for few properties that the app needs.
  - `index.html`: The minimal starting HTML that contains the title of the site, a Google Fonts link, a div where the application/site content will be injected into, and 2 script tags that include the Modernizr build and application Javascript. This is all the HTML that will be served to the client browser, all the rest will be generated and injected by React (see [Notes on progressive enhancement](#notes-on-progressive-enhancement)).
  - `fonts/`: Currently contains all the icon fonts. The Lato font is loaded via Google Fonts. The file names are hashed for caching.
  - `images/`: Larger images will be hashed and included here. Smaller images will be turned into base64 data URIs automatically by Webpack to minimize the number of HTTP requests.

#### Application structure

The following diagram outlines the basic flow that connects different parts of the application.

[](diagram.png)

#### Notes on progressive enhancement

Currently the initial HTML served to the client is very minimal — only one div excluding all the style and script tags. All the contents will be generated by React and injected into that one div after React has been loaded. This may raise some concerns in terms of progressive enhancement.

What if the client browser has Javascript disabled? What happens when the script download is interrupted or failed? Wouldn’t it give the client an even faster load time and “safer” first load if all the contents are already included in the initial HTML request? These are all legitimate concerns and have been taken into consideration.

Typically in a React application that needs to meet these requirements, the server will have already rendered and cached a static HTML version of the contents. That way, the initial HTML payload will include all the contents. And once React loads, it will bind to the already loaded static HTML to resume providing the “fancier” functionality. This way, developers and users get the best of both worlds.

And of course, due to the scope of this project, there will not be a static HTML version that is served by the server on initial request — there will not be a server at all.

#### Development

**Running (in development mode)**

`git clone https://bitbucket.org/samzhao/singing-success-academy `

`npm install && npm start`

**Building (in production mode)**

`npm install && npm run build`

#### Todos

- [ ] Fully responsive support with media queries and classes, e.g. `col-lg visible-xs`
- [ ] Implement the percent grid as fallback for flexbox
- [ ] More tests across devices and screen sizes
- [ ] More realistic dummy content population
- [ ] More realistic copywriting
- [ ] Data storage of some sort (localStorage, IndexDB, SQLite, PostgresQL), instead of using JSON mock
