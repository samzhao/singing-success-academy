import { createStore, applyMiddleware } from 'redux'
import ReactRouterRedux, { routerMiddleware } from 'react-router-redux'

export default function configureStore(history, data) {
  const reduxRouterMiddleware = routerMiddleware(history)

  const finalCreateStore = applyMiddleware(...[reduxRouterMiddleware])(createStore)

  const reducer = require('./modules').default
  const store = finalCreateStore(reducer, data)

  if (process.env !== 'production' && module.hot) {
    module.hot.accept('./modules', () => {
      store.replaceReducer(require('./modules').default)
    })
  }

  return store
}
