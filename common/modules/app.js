import createConstants from 'flux-constants'
import { Set, Map } from 'immutable'
import { LESSONS } from '../../src/constants'
import { assign } from 'lodash'

const constants = createConstants([
  "ADD_TO_CART",
  "REMOVE_FROM_CART",
  "TOGGLE_CART",
  "TOGGLE_NAV",
  "UPDATE_USER_PROFILE"
])

const initialState = new Map({
  isCartVisible: false,
  isNavCollapsed: true,
  lessons: LESSONS,
  cartItems: new Set(),
  userProfile: {
    firstName: '',
    lastName: '',
    email: '',
    streetNumber: '',
    streetName: '',
    province: 'bc',
    country: 'canada',
    shippingMethod: 'standard',
    city: '',
    zip: '',
    comment: ''
  }
})

export default function reducer(state = initialState, action = {}) {
  let cartItems = state.get('cartItems')
  switch(action.type) {
    case constants.TOGGLE_NAV:
      const isNavCollapsed = !state.get('isNavCollapsed')
      return state.set('isNavCollapsed', isNavCollapsed)
    case constants.TOGGLE_CART:
      const isCartVisible = action.isVisible !== undefined ? action.isVisible : !state.get('isCartVisible')
      return state.set('isCartVisible', isCartVisible)
    case constants.ADD_TO_CART:
      cartItems = cartItems.concat(action.item)
      return state.set('cartItems', cartItems)
    case constants.REMOVE_FROM_CART:
      cartItems = cartItems.delete(action.item)
      return state.set('cartItems', cartItems)
    case constants.UPDATE_USER_PROFILE:
      const userProfile = assign(state.get('userProfile'), action.profile)
      return state.set('userProfile', userProfile)
    default:
      return state
  }
}

export function addToCart(item) {
  return {
    type: constants.ADD_TO_CART,
    item
  }
}

export function removeFromCart(item) {
  return {
    type: constants.REMOVE_FROM_CART,
    item
  }
}

export function toggleNav(isCollapsed) {
  return {
    type: constants.TOGGLE_NAV,
    isCollapsed
  }
}

export function toggleCart(isVisible) {
  return {
    type: constants.TOGGLE_CART,
    isVisible
  }
}

export function updateUserProfile(profile) {
  return {
    type: constants.UPDATE_USER_PROFILE,
    profile
  }
}
