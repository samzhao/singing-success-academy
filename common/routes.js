import React from 'react'
import { IndexRoute, Route, Redirect } from 'react-router'
import Layout from '../src/components/Layout'
import {
  HomePage,
  LessonsPage,
  LessonDetailPage,
  LessonListingPage,
  NewsPage,
  AboutPage,
  ContactPage,
  CheckoutPage,
  CartPage,
  PaymentPage,
  InvoicePage
} from '../src/pages'

export default (
  <Route component={Layout}>
    <Route path="/" component={HomePage}/>

    <Route path="lessons" component={LessonsPage}>
      <IndexRoute component={LessonListingPage}/>
      <Route path=":id" component={LessonDetailPage} />
    </Route>
    <Route path="news" component={NewsPage} />
    <Route path="about" component={AboutPage} />
    <Route path="contact" component={ContactPage} />
    <Route path="checkout" component={CheckoutPage}>
      <IndexRoute component={CartPage}/>
      <Route path="payment" component={PaymentPage}/>
      <Route path="invoice" component={InvoicePage} />
    </Route>

    <Redirect to="/home" path="/" />
  </Route>
)
