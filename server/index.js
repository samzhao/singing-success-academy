import express from 'express'
import fs from 'fs'
import ejs from 'ejs'
import React from 'react'
import { renderToString } from 'react-dom/server'
import configureStore from '../common/configureStore'
import compression from 'compression'
import favicon from 'serve-favicon'
import path from 'path'
import http from 'http'
import PrettyError from 'pretty-error'

import { match, RouterContext } from 'react-router'
import { syncHistoryWithStore } from 'react-router-redux'
import createHistory from 'react-router/lib/createMemoryHistory'
import { Provider } from 'react-redux'
import routes from '../common/routes'

ejs.delimiter = '$'

const pretty = new PrettyError()
const app = new express()
const server = new http.Server(app)

const PORT = process.env.PORT || 3000

app.use(compression())
app.use(favicon('build/icons/favicon.ico'))
app.use(express.static('build', { index: false }))

function renderApp(props, res, store) {
  const component = (
    <Provider store={store} key="provider">
      <RouterContext {...props}/>
    </Provider>
  )
  const template = fs.readFileSync('build/index.html').toString()
  const content = renderToString(component)
  const markup = ejs.render(template, { content })

  res.send(markup)
}

app.use((req, res) => {
  const memoryHistory = createHistory(req.originalUrl)
  const store = configureStore(memoryHistory)
  const history = syncHistoryWithStore(memoryHistory, store)

  match({ history, routes: routes, location: req.originalUrl }, (error, redirectLocation, renderProps) => {
    if (redirectLocation) {
      const { pathname, search, hash } = redirectLocation
      res.redirect(pathname+search+hash)
    } else if (error) {
      console.error('ROUTER ERROR:', pretty.render(error))
      res.status(500)
      res.write('Server Error!')
      res.end()
    } else if (renderProps) {
      global.navigator = { userAgent: req.headers['user-agent'] }
      renderApp(renderProps, res, store)
    } else {
      res.status(404).send('Not Found')
    }
  })
})

server.listen(PORT, (err) => {
  if (err)
    console.error(err)
  console.info(`App running on port ${PORT}`)
})
