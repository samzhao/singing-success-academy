import React from 'react'
import DOM from 'react-dom'

import { Provider } from 'react-redux'
import configureStore from '../common/configureStore'
import { Router, browserHistory } from 'react-router'
import { syncHistoryWithStore } from 'react-router-redux'
import routes from '../common/routes'

const store = configureStore(browserHistory, window.__DATA)
const history = syncHistoryWithStore(browserHistory, store)

require('normalize.css/normalize.css')
require('./styles/app.scss')

const entryPoint = document.getElementById('app')

DOM.render(
  <Provider store={store} key="provider">
    <Router history={history} routes={routes} />
  </Provider>,
  entryPoint
)
