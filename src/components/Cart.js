import React, { Component } from 'react'
import autobind from 'autobind-decorator'
import { Row, Col } from './Grid'
import { Link } from 'react-router'
import Icon from './Icon'
import QuantityCounter from './QuantityCounter'

@autobind
export default class Cart extends Component {
  removeItem(item, e) {
    e.preventDefault()
    const { actions } = this.props
    actions.removeFromCart(item)
  }

  onSelectQuantity(item, e) {
    const { actions } = this.props
    const val = parseInt(e.target.value, 10)
    item.quantity = val
    actions.addToCart(item)
  }

  render() {
    const { style, className="", store, canRemove=true, editable=true } = this.props
    let items = this.props.items || []

    if (store) {
      items = store.app.get('cartItems').toJS()
    }

    const total = items.reduce((prev, curr) => {
      const price = curr.price
      const quantity = curr.quantity
      prev += price * quantity
      return prev
    }, 0)

    const noItems = items.length < 1

    const cartItems = items.map(item => (
      <li key={item.id}>
        <Row className="grid--no-wrap grid--align-center">
          <Col count="3">
            <figure>
              <img src={item.img} width="100%" style={{minWidth: 50}} alt={item.title} />
            </figure>
          </Col>
          <Col count={canRemove ? '8' : '9'}>
            <strong style={{marginBottom: '1em'}}>{ item.title }</strong>
            <QuantityCounter editable={editable} styled={false} price={item.price} count={item.quantity} onSelectQuantity={this.onSelectQuantity.bind(this, item)}></QuantityCounter>
          </Col>
          {
            canRemove &&
            <Col count="1" className="grid--align-self-center" align="center" style={{padding: 0}}>
              <a href="#" className="btn-close hint--bottom-left hint--rounded hint--small" aria-label="Remove" onClick={this.removeItem.bind(this, item)}>
                <Icon type="ios-close" />
              </a>
            </Col>
          }
        </Row>
      </li>
    ))

    return (
      <div className={`cart ${className}`} style={style}>
        <ul>
          { cartItems }
          { noItems && <p style={{padding: '0 16px'}}>Your Lesson List is empty.<br/><Link className="list-link" to="/lessons">Go Add Some.</Link></p> }
        </ul>
        <hr style={{margin: '0 16px', border: 0, borderTop: '1px solid #eee'}}/>
        <p style={{textTransform: 'uppercase'}}>Total: ${total}</p>
        <Link to="/checkout" className="btn btn-primary btn-sm btn-square full-width btn-lesson-list">Go Checkout</Link>
      </div>
    )
  }
}
