import React from 'react'
import Icon from './Icon'
import ReactCSSTransitionGroup from 'react-addons-css-transition-group'

export default ({ onClick, number=0 }) => (
  <a href="#" onClick={onClick} className={(number ? 'has-items' : '') + ' hint--bottom-left hint--rounded hint--medium'} aria-label="Lesson List">
    <Icon type="md-book" />
    <ReactCSSTransitionGroup
      transitionName={{
        enter: 'animated',
        enterActive: 'bounce',
        leave: 'fade-leave',
        leaveActive: 'fade-leave-active'
      }}
      transitionEnterTimeout={2500}
      transitionLeaveTimeout={500}
    >
      { Boolean(number) && <span className="cart-badge">{number}</span> }
    </ReactCSSTransitionGroup>
  </a>
)
