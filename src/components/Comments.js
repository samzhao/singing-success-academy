import React from 'react'
import { random } from 'lodash'
import Star from './Star'
import { Row, Col } from './Grid'
import Icon from './Icon'

export default ({ title="Comments", comments=[] }) => {
  return (
    <div className="comments">
      <h3 className="align-center" style={{fontWeight: 'normal'}}>{ title }</h3>

      <ul>
        {
          comments.map(comment => {
            const { id, username="Anonymous", rating, content } = comment

            return (
              <li key={id}>
                <div className="username">{username}</div>
                <Row>
                  <Col collapseOn="sm" count="1">
                    <figure className="avatar">
                      <Icon type="ios-megaphone"/>
                    </figure>
                  </Col>
                  <Col collapseOn="sm" count="11">
                    <strong>Rating: <Star rating={rating}/></strong>
                    <article className="speech">
                      { content }
                    </article>
                  </Col>
                </Row>
              </li>
            )
          })
        }
      </ul>
    </div>
  )
}
