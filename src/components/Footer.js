import React from 'react'
import { Row, Col } from './Grid'
import Nav from './Nav'

export default () => (
  <footer className="align-center">
    <div className="container">
      <Row style={{textAlign:'center'}}>
        <Col collapseOn="sm" className="copyright-info grid--justify-center grid-align-center">
          Copyright © 2016. Singing Success Academy. All rights reserved.
        </Col>
        <Col collapseOn="sm" className="footer-nav">
          <Nav phone={false} logo={false} fixed={false} cart={false} />
        </Col>
      </Row>
    </div>
  </footer>
)
