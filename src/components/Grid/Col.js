import React, { Component } from 'react'

export default class Col extends Component {
  render() {
    const { count=0, align="baseline", collapseOn="", component='div', children, className='', style={} } = this.props
    const classnames = `grid__col-${collapseOn === "" ? '' : collapseOn+'-'}${count || 'auto'} grid--align-${align} grid--align-self-space-between ${className}`

    return React.createElement(component, { className: classnames, style }, [children])
  }
}
