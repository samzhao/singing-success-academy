import React, { Component } from 'react'

export default class Row extends Component {
  render() {
    const { component='div', children, className='' } = this.props
    const classnames = `grid ${className}`

    return React.createElement(component, { className: classnames }, [children])
  }
}
