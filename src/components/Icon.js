import React from 'react'

export default ({ type='help', component='i', children, className='', style }) => {
  // state could be 'color' or 'outline'
  const classnames = `icon ion-${type} ${className}`

  return React.createElement(component, { className: classnames, style }, [])
}
