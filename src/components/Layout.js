import React, { PropTypes, Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import * as appActions from '../../common/modules/app'
import Nav from './Nav'
import Footer from './Footer'
import ReactCSSTransitionGroup from 'react-addons-css-transition-group'

@connect(
  state => ({store: state}),
  dispatch => ({actions: bindActionCreators(appActions, dispatch)})
)
export default class Layout extends Component {
  static contextTypes = {
    store: PropTypes.object.isRequired
  }

  render() {
    const { children, location, store, actions } = this.props

    return (
      <main>
        <Nav store={store} actions={actions} />

        <ReactCSSTransitionGroup
          component="content"
          transitionName="fade"
          transitionEnterTimeout={1000}
          transitionLeaveTimeout={500}>
          {
            React.cloneElement(children, {
              key: location.pathname,
              store, actions
            })
          }
        </ReactCSSTransitionGroup>

        { location.pathname !== '/' && <Footer /> }
      </main>
    )
  }
}
