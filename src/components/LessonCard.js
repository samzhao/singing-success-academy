import React, { Component } from 'react'
import { Link } from 'react-router'
import { Row, Col } from '../components/Grid'
import Star from './Star'

export default class LessonCard extends Component {
  truncate(text, max) {
    if (text.length <= max)
      return text

    return text.substring(0, max - 3) + '...'
  }

  render() {
    const {img="http://placehold.it/300x200", id=1, title="Card Title", desc="Card Desc", rating=0, disabled=false} = this.props

    return (
      <div className={`lesson-card vertical ${disabled ? 'disabled' : ''}`}>
        <Link to={`/lessons/${id}`}>
          <figure className="card-media" style={{
            backgroundImage: `url(${img})`
          }} title={title}>
          </figure>
        </Link>

        <article className="card-content">
          <Row>
            <Col count="12" component="h4" className="card-title">{title}</Col>
            <Col count="12" className="card-rating align-right">
              <Star rating={rating} />
            </Col>

          </Row>
          <p>{this.truncate(desc, 100)}</p>

          <Link to={`/lessons/${id}`} className="btn btn-primary full-width align-center">LEARN MORE</Link>
        </article>
      </div>
    )
  }
}
