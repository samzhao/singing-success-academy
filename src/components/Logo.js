import React from 'react'

export default ({ state='', hover=false, component='div', children, className='' }) => {
  // state could be 'color' or 'outline'
  const classnames = `logo ${hover ? 'hover' : ''} ${state} ${className}`

  return React.createElement(component, { className: classnames }, [])
}
