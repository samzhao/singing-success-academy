import React, { Component } from 'react'
import autobind from 'autobind-decorator'
import { isEmpty } from 'lodash'
import { findDOMNode } from 'react-dom'
import { Row, Col } from './Grid'
import Logo from './Logo'
import Icon from './Icon'
import { Link } from 'react-router'
import CartIcon from './CartIcon'
import Cart from './Cart'

import onClickOutSide from 'react-onclickoutside'

@autobind
class Nav extends Component {
  handleClickOutside(e) {
    const { actions } = this.props

    if (!isEmpty(actions)) {
      actions.toggleCart(false)
    }
  }

  toggleNav(e) {
    e.preventDefault()

    const { actions } = this.props
    actions.toggleNav()
  }

  toggleCart(e) {
    e.preventDefault()

    const { actions } = this.props
    actions.toggleCart()
  }

  render() {
    const {phone=true, logo=true, fixed=true, cart=true, store, actions} = this.props
    let cartItems = []
    let isCartVisible = false
    let isNavCollapsed = true

    if (store) {
      cartItems = store.app.get('cartItems').toJS()
      isCartVisible = store.app.get('isCartVisible')
      isNavCollapsed = store.app.get('isNavCollapsed')
    }

    const navHorizontal = (
      <Row component="ul" className={`nav grid--align-center grid--no-wrap ${isNavCollapsed ? '' : 'show'}`}>
        <Col component="li">
          <Link to="/" activeClassName="active">Home</Link>
        </Col>
        <Col component="li">
          <Link to="/lessons" activeClassName="active">Lessons</Link>
        </Col>
        <Col component="li" className="grid--no-collapse">
          <Link to="/news" activeClassName="active">News & Events</Link>
        </Col>

        {
          logo && (
            <Col component="li" className="nav-logo">
              <Link to="/">
                <Logo hover="true" />
              </Link>
            </Col>
          )
        }

        <Col component="li">
          <Link to="/about" activeClassName="active">About</Link>
        </Col>
        <Col component="li">
          <Link to="/contact" activeClassName="active">Contact</Link>
        </Col>
        {
          phone && (
            <Col component="li" className="phone grid--no-collapse">
              <a href="tel:604-123-1234">
                <Icon type="ios-call" /> 604-314-1592
              </a>
            </Col>
          )
        }
        {
          cart && (
            <Col component="li" count="1" className="nav-icon-btn cart-icon">
              <CartIcon number={cartItems.length} onClick={this.toggleCart}/>
              <Cart store={store} actions={actions} items={cartItems} className={isCartVisible ? '' : 'hidden'} ref="cart" />
            </Col>
          )
        }
      </Row>
    )

    const navCollapsable = (
      <Row component="ul" className={`nav-collapsed grid--align-center grid--no-wrap ${fixed ? 'hidden' : ''}`}>
        <Col component="li" count="1">
          <a href="#" onClick={this.toggleNav} className="nav-icon-btn">
            <Icon type="ios-menu" />
          </a>
        </Col>
        <Col component="li" count="10" className="nav-logo">
          <Link to="/">
            <Logo hover="true" />
          </Link>
        </Col>
        <Col component="li" count="1" className="nav-icon-btn cart-icon">
          <CartIcon number={cartItems.length} onClick={this.toggleCart}/>
          <Cart store={store} actions={actions} items={cartItems} className={isCartVisible ? '' : 'hidden'} ref="cart" />
        </Col>
      </Row>
    )

    return (
      <nav className={fixed ? 'fixed' : ''}>
        <div className="container">
          { navHorizontal }
          { navCollapsable }
        </div>
      </nav>
    )
  }
}

export default onClickOutSide(Nav)
