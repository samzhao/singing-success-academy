import React, { Component } from 'react'
import { Row, Col } from './Grid'
import autobind from 'autobind-decorator'
import { isEqual } from 'lodash'

const countries = [
  {
    value: 'usa',
    label: 'USA'
  }, {
    value: 'canada',
    label: 'Canada'
  }, {
    value: 'japan',
    label: 'Japan'
  }, {
    value: 'china',
    label: 'China'
  }, {
    value: 'india',
    label: 'India'
  }
]
const provinces = [
  {
    value: 'ab',
    label: 'AB'
  }, {
    value: 'bc',
    label: 'BC'
  }, {
    value: 'nb',
    label: 'NB'
  }, {
    value: 'ns',
    label: 'NS'
  }, {
    value: 'nl',
    label: 'NL'
  }, {
    value: 'mb',
    label: 'MB'
  }
]

@autobind
export default class PaymentForm extends Component {
  onSubmit(e) {
    e.preventDefault()

    this.props.history.push('/checkout/invoice')
  }

  onChange(e) {
    const { userProfile, actions } = this.props
    const { name, value } = e.target

    this.forceUpdate()
    actions.updateUserProfile({ [name]: value })
  }

  render() {
    const { userProfile } = this.props
    const {
      firstName,
      lastName,
      email,
      streetNumber,
      streetName,
      city,
      province,
      country,
      zip,
      shippingMethod,
      comment
    } = userProfile

    return (
      <form className="payment" onSubmit={this.onSubmit}>
        <fieldset>
          <legend>Contact Details</legend>
          <Row>
            <Col count="4" collapseOn="sm">
              <label>
                <span>First Name</span>
                <input value={firstName} onChange={this.onChange} type="text" name="firstName" placeholder="John" />
              </label>
            </Col>
            <Col count="4" collapseOn="sm">
              <label>
                <span>Last Name</span>
                <input value={lastName} onChange={this.onChange} type="text" name="lastName" placeholder="Doe" />
              </label>
            </Col>
            <Col count="4" collapseOn="sm">
              <label>
                <span>E-Mail</span>
                <input value={email} onChange={this.onChange} type="email" name="email" placeholder="john@example.com" />
              </label>
            </Col>
          </Row>
        </fieldset>
        <fieldset>
          <legend>Delivery Details</legend>
          <Row>
            <Col count="6" collapseOn="sm">
              <label>
                <span>Street Number</span>
                <input value={streetNumber} onChange={this.onChange} type="text" name="streetNumber" placeholder="Apt. suite or unit number"/>
              </label>
            </Col>
            <Col count="6" collapseOn="sm">
              <label>
                <span>Street Name</span>
                <input value={streetName} onChange={this.onChange} type="text" name="streetName" placeholder="Street Name"/>
              </label>
            </Col>
          </Row>
          <Row>
            <Col>
              <label>
                <span>Country</span>
                <select name="country" value={country} onChange={this.onChange}>
                  {
                    countries.map((opt, i) => (
                      <option value={opt.val} key={i}>{opt.label}</option>
                    ))
                  }
                </select>
              </label>
            </Col>
          </Row>
          <Row>
            <Col count="6" collapseOn="sm">
              <label>
                <span>City</span>
                <input value={city} onChange={this.onChange} type="text" name="city" placeholder="City"/>
              </label>
            </Col>
            <Col count="3" collapseOn="sm">
              <label>
                <span>Province</span>
                <select name="province" value={province} onChange={this.onChange}>
                  {
                    provinces.map((opt, i) => (
                      <option value={opt.val} key={i}>{opt.label}</option>
                    ))
                  }
                </select>
              </label>
            </Col>
            <Col count="3" collapseOn="sm">
              <label>
                <span>Zip Code</span>
                <input value={zip} onChange={this.onChange} type="text" name="zip" placeholder="V1A 1A1"/>
              </label>
            </Col>
          </Row>
        </fieldset>
        <fieldset>
          <legend>Credit Card Details</legend>
          <Row>
            <Col collapseOn="sm" count="4">
              <label>
                <span>Name On Card</span>
                <input type="text" placeholder="John Doe"/>
              </label>
            </Col>
            <Col collapseOn="sm" count="5">
              <label>
                <span>Card Number</span>
                <input type="text" placeholder="Credit card number"/>
              </label>
            </Col>
            <Col collapseOn="sm" count="3">
              <label>
                <span>CVV Code</span>
                <input type="number" placeholder="123"/>
              </label>
            </Col>
          </Row>
        </fieldset>
        <fieldset>
          <legend>Shipping Method</legend>
          <Row className="radio-group">
            <Col>
              <label>
                <input checked={shippingMethod === 'standard'} onChange={this.onChange} type="radio" name="shippingMethod" value="standard"/>
                <span>
                  <b>Standard:</b> In 3-6 working days.
                </span>
              </label>
            </Col>
            <Col>
              <label>
                <input checked={shippingMethod === 'express'} onChange={this.onChange} type="radio" name="shippingMethod" value="express"/>
                <span>
                  <b>Express:</b> In 1-2 working days.
                </span>
              </label>
            </Col>
          </Row>
          <Row>
            <Col>
              <label>
                <span>Additional Comments</span>
                <textarea name="comment" value={comment} onChange={this.onChange} cols="30" rows="5" placeholder="Additional comments and instructions"></textarea>
              </label>
            </Col>
          </Row>
        </fieldset>
        <Row>
          <Col>
              <button type="submit" className="btn btn-primary full-width confirm-order">Confirm Order</button>
          </Col>
        </Row>
      </form>
    )
  }
}
