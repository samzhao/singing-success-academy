import React from 'react'

export default ({ price=0, min=0, max=999, count=0, onSelectQuantity, styled=true, editable=true }) => (
  <div className={`quantity-counter ${styled ? 'styled' : ''}`}>
    <span className="price">
      ${price}{ " x " }
      {
        editable ? <input type="number" min={min} max={max} defaultValue={count} onChange={onSelectQuantity} /> : count
      }
    </span>
    <span style={{fontSize:"0.8em"}}> {count > 1 ? 'lessons' : 'lesson'}</span>
  </div>
)
