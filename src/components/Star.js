import React from 'react'
import Icon from './Icon'

export default ({total=5, rating=0}) => {
  let fullStars = []
  let emptyStars = []

  for (var i = rating - 1; i >= 0; i--) {
    fullStars.push(
      <Icon type="md-star" className="full-star" key={"full " + i}/>
    )
  }

  for (var i = (total - rating) - 1; i >= 0; i--) {
    emptyStars.push(
      <Icon type="md-star" className="empty-star" key={"empty " + i} />
    )
  }

  let stars = fullStars.concat(emptyStars)

  return (
    <span className="star">
      { stars }
    </span>
  )
}
