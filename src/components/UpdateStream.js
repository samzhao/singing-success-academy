import React from 'react'
import { Row, Col } from './Grid'
import { range } from 'lodash'

const dummyContent = range(3).map((v, i) => ({id: i+1, title: "Lorem ipsum dolor.", content: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ducimus ipsum, doloremque neque facere. Ipsa, neque."}))
let content = []

content = dummyContent.concat([
  {
    id: dummyContent.length+1,
    title: "Academy New Years Concert!",
    content: "We welcome all our coaches, students, and staff members to join our New Years Concert! Tickets are available at the reception."
  },
  {
    id: dummyContent.length+2,
    title: "Fort McMurray Fundraising Concert",
    content: "Give the people at Fort McMurray a hand by donating or performing at the Fundraising Concert next Tuesday!"
  },
  {
    id: dummyContent.length+3,
    title: "New Course Open!",
    content: "We are thrilled to announce a new course open for registration: <a href='/lessons/2'>Male Rock Singer Starter</a>!"
  }
]).reverse()

export default () => (
  <div className="update-stream">
    {
      content.map(c => (
        <div className="update" key={c.id}>
          <Row>
            <Col collapseOn="md" count="1" className="grid--justify-center">
              <figure className="update-media"></figure>
            </Col>
            <Col collapseOn="md" count="11">
              <article className="update-content">
                <h4 className="update-title">{c.title}</h4>
                <p dangerouslySetInnerHTML={{__html:c.content}} />
              </article>
            </Col>
          </Row>
        </div>
      ))
    }
  </div>
)
