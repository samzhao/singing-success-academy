import React from 'react'

export default ({img="https://s3.amazonaws.com/uifaces/faces/twitter/_everaldo/128.jpg", name="John Doe", singer="Rock", quote="Lorem ipsum dolor sit amet."}) => (
  <div className="user-card align-center">
    <figure className="avatar" style={{
      backgroundImage: `url(${img})`
    }}>
    </figure>

    <div className="profile">
      <div className="name">{name}</div>
      <div className="singing-style">
        {singer} Singer
      </div>
    </div>

    <div className="quote">
      {quote}
    </div>
  </div>
)
