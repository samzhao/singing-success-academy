var female_opera_singer = require('./assets/female_opera_singer.jpg')
var male_country_singer = require('./assets/male_country_singer.jpg')
var male_pop_singer = require('./assets/male_pop_singer.jpg')
var male_rock_singer = require('./assets/male_rock_singer.jpg')

function resolveImage(url) {
  return process.env.BROWSER ? url : '/'+url
}

var LESSONS = [{
  id: 1,
  title: "Male Pop Singer Advanced",
  desc: "Tought by the top vocal coaches in Canada who have personally worked with singers like Prince, Justin Bieber, Bruno Mars, and others. Dr. Seth Riggs, Brett Manning, and load vocalist Hayley Williams will work with you 1-on-1 to improve your vocal abilities and maximize your potentials!",
  whosFor: "You have done our Male Pop Singer Intermediate course (FULL)\n\
  You have had extensive vocal trainings before\n\
  You are ready to become a pop rockstar!",
  img: resolveImage(male_pop_singer),
  price: 210,
  comments: [{
    id: 1,
    username: "John D",
    content: "Dr. Seth Riggs helped me discover my potential of singing higher than normal notes, which is quite amazing! However, I can't seem to sustain those high notes. Perhaps I need to book more lessons with him.",
    rating: 4
  }, {
    id: 2,
    username: "Kevin D",
    content: "Brett is a very nice vocal teacher. He appears to know what he's talking about, and his skills are without a doubt phenomenal. That being said, I just don't find his method applies to me somehow.",
    rating: 3
  }]
}, {
  id: 2,
  title: "Male Rock Singer Starter",
  desc: "Ever wanted to sing like a rockstar, but don't know how to control your voice and use it fully? If you answer with a resounding 'yes', then you shouldn't hesitate to try out this lesson! Canada-renown singer and song writer Freddie McCartney will personally guide you through the discovery and training process to kick start your rockstar career!",
  whosFor: "Anyone who wants to sing rock!\n\
  You want to discover the potentials of your vocal ability\n\
  You have not had much vocal training before",
  img: resolveImage(male_rock_singer),
  price: 100,
  comments: [{
    id:1,
    content: "Wow! I didn't know that I could sing this well before I got into the course :o",
    rating: 5
  }, {
    id: 2,
    username: "Joe",
    rating: 3,
    content: "I know I could sing, yet I paid the academy to tell me in a nicer way ¯\_(ツ)_/¯. That said, there are some helpful tips in there."
  }]
}, {
  id: 3,
  title: "Female Opera Singer Intermediate",
  desc: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Assumenda nihil laborum harum vitae sequi reprehenderit minus voluptates, maxime neque perferendis expedita quis suscipit, nulla sed non ab at nemo! At.",
  img: resolveImage(female_opera_singer),
  price: 70,
  disabled: true,
  comments: [{
    id:1,
    rating: 5
  }]
}, {
  id: 4,
  title: "Male Country Singer 1on1",
  desc: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quam modi voluptate eius nostrum provident, rerum adipisci eos! Officia quam molestiae autem earum obcaecati, repellendus voluptatem deserunt possimus commodi aspernatur eum?",
  img: resolveImage(male_country_singer),
  price: 50,
  disabled: true,
  comments: [{
    id:1,
    rating: 3
  }]
}]

module.exports = {
  siteName: 'Singing Success Academy',
  LESSONS
}
