import React from 'react'
import Title from 'react-document-title'
import { siteName } from 'constants'
import { Link } from 'react-router'
import { Row, Col } from '../components/Grid'
import UserCard from '../components/UserCard'

const header_img = require('../assets/male_rock_singer.jpg')

export default () => (
  <Title title={ `${siteName} | About` }>
    <section className="about-page">
      <div className="container">
        <article>
          <h2 className="section-header align-center">Who We Are</h2>
          <p>
            <figure style={{
              marginBottom: 15
            }}>
              <img src={header_img} width="100%" alt="Our History"/>
            </figure>
            Leverage agile frameworks to provide a robust synopsis for high level overviews. Iterative approaches to corporate strategy foster collaborative thinking to further the overall value proposition. Organically grow the holistic world view of disruptive innovation via workplace diversity and empowerment.
          </p>
          <p>
            Bring to the table win-win survival strategies to ensure proactive domination. At the end of the day, going forward, a new normal that has evolved from generation X is on the runway heading towards a streamlined cloud solution. User generated content in real-time will have multiple touchpoints for offshoring.
          </p>
          <p className="small">
            Generated with http://www.cipsum.com/
          </p>
        </article>

        <article>
          <h2 className="align-center">Why Us</h2>

          <p>
            Ramen long tail crowdfunding. Network effects ownership user experience. Early adopters long tail rockstar niche market marketing client paradigm shift. Creative handshake launch party stealth prototype assets responsive web design pivot. Termsheet crowdsource return on investment vesting period ownership series A financing freemium angel investor. Gen-z handshake facebook incubator freemium business model canvas hypotheses.
          </p>
          <p>
            Hackathon value proposition monetization burn rate business-to-business technology iteration alpha. Startup user experience non-disclosure agreement direct mailing learning curve partner network rockstar market. Infographic paradigm shift ecosystem responsive web design validation first mover advantage MVP user experience social proof technology series A financing. Hypotheses seed round user experience success. Network effects technology graphical user interface business-to-consumer backing hypotheses venture business plan iteration accelerator. Scrum project paradigm shift android.
          </p>
          <p className="small">
            Generated with http://startupsum.com/
          </p>
        </article>

        <article style={{padding: 0}}>
          <h2 className="align-center">Our Hall Of Fame</h2>

          <Row>
            <Col collapseOn="sm" count="3">
              <UserCard name="Sauro" quote="When I started Reynholm Industries, I had just two things in my possession: a dream and 6 million pounds." img="https://s3.amazonaws.com/uifaces/faces/twitter/sauro/128.jpg"/>
            </Col>

            <Col collapseOn="sm" count="3">
              <UserCard name="Erondu" quote="Phenomenal lessons! Just phenomenal!" singer="Pop" img="https://s3.amazonaws.com/uifaces/faces/twitter/erondu/128.jpg"/>
            </Col>

            <Col collapseOn="sm" count="3">
              <UserCard name="Esther Crawford" quote="Dear Sir stroke Madam, I am writing to inform you of a fire which has broken out at the premises of..." singer="Opera" img="https://s3.amazonaws.com/uifaces/faces/twitter/esthercrawford/128.jpg" />
            </Col>

            <Col collapseOn="sm" count="3">
              <UserCard name="Stacey Camp" quote="You mean 999. Yes, yes, I mean 999! Yeah, I know. That's the American one, you berk!" singer="Country" img="https://s3.amazonaws.com/uifaces/faces/twitter/stylecampaign/128.jpg" />
            </Col>
          </Row>
        </article>
      </div>
      <div className="call-to-action">
        <div className="container">
          <Row>
            <Col align="center">
              <h2>Be The Next Household Name</h2>
              <Link to="/lessons" className="btn btn-lg btn-primary">Get Started Today!</Link>
            </Col>
          </Row>
        </div>
      </div>
    </section>
  </Title>
)
