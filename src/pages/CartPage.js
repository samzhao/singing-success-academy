import React, { Component } from 'react'
import Title from 'react-document-title'
import { Link } from 'react-router'
import { siteName } from 'constants'

import { Row, Col } from '../components/Grid'
import Cart from '../components/Cart'

export default class CartPage extends Component {
  render() {
    const { actions, store } = this.props
    const cartItems = store.app.get('cartItems').toJS()

    const checkoutLink = (
      <Row className="grid--justify-end">
        <Col align="end">
          <Link className="btn btn-primary" to="/checkout/payment">Proceed To Checkout</Link>
        </Col>
      </Row>
    )

    return (
      <div className="cart-page">
        <h2 className="section-header">Checkout</h2>
        <Cart store={store} actions={actions} className="align-center" />
        { cartItems.length > 0 && checkoutLink }
      </div>
    )
  }
}
