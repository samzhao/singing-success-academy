import React, { Component } from 'react'
import Title from 'react-document-title'
import { siteName } from 'constants'
import ReactCSSTransitionGroup from 'react-addons-css-transition-group'

export default class CheckoutPage extends Component {
  componentDidMount() {
    const { actions } = this.props
    actions.toggleCart(false)
  }

  render() {
    const { actions, store, children, location } = this.props

    return (
      <Title title={ `${siteName} | Checkout` }>
        <section className="checkout-page">
          <div className="container">
            <article>
              <content style={{display: 'block', padding: '0 15px'}}>
                {
                  React.cloneElement(children, {
                    key: location.pathname,
                    store, actions, ...this.props
                  })
                }
              </content>
            </article>
          </div>
        </section>
      </Title>
    )
  }
}
