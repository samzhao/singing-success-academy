import React, { Component } from 'react'
import Title from 'react-document-title'
import { siteName } from 'constants'
import { Row, Col } from '../components/Grid'
import Icon from '../components/Icon'

export default class ContactPage extends Component {
  onSubmit(e) {
    e.preventDefault()

    alert('Thank you for your feedback! We will get back to you soon.')
  }

  render() {
    return(
      <Title title={ `${siteName} | Contact Us` }>
        <section className="contact-page">
          <article className="container">
            <h2 className="section-header">Contact Us</h2>

            <Row>
              <Col collapseOn="xs" count="6">
                <strong>Send Us A Message</strong>

                <form onSubmit={this.onSubmit}>
                  <Row component="article">
                    <Col>
                      <label>
                        <span>First Name:</span>
                        <input type="text" placeholder="First Name"/>
                      </label>
                    </Col>
                    <Col>
                      <label>
                        <span>Last Name:</span>
                        <input type="text" placeholder="Last Name"/>
                      </label>
                    </Col>
                  </Row>

                  <article>
                    <label>
                      <span>Subject:</span>
                      <input type="text" placeholder="Subject"/>
                    </label>
                  </article>

                  <article>
                    <label>
                      <span>Content:</span>
                      <textarea cols="30" rows="10" placeholder="Content"></textarea>
                    </label>
                  </article>

                  <button type="submit" className="btn btn-primary full-width">Send Feedback</button>
                </form>
              </Col>
              <Col collapseOn="xs" count="6">
                <strong>Address</strong>
                <div className="address">
                  201 - 1357 Canada Place., <br/>
                  Vancouver, B.C., <br/>
                  V1X 1Y1
                </div>

                <div style={{width: '100%'}}>
                  <a href="tel:604-314-1592">
                    <Icon type="ios-call" /> 604-314-1592
                  </a> <br/>

                  <a href="mailto:info@example.com">
                    <Icon type="ios-mail" /> info@singingsuccess.com
                  </a>

                  <strong>Direction</strong>
                  <div className="media-container">
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1301.1701020925007!2d-123.1122098785813!3d49.288898256629494!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x5486719d24e2e021%3A0xb7057fe085c86109!2sCanada+Place!5e0!3m2!1sen!2sca!4v1465864893165" width="450" height="300" frameborder="0" style={{border:0}} allowfullscreen>
                      <a href="https://www.google.ca/maps/place/999+Canada+Pl,+Vancouver,+BC+V6C+3E1/@49.2879437,-123.1152408,17z/data=!3m1!4b1!4m5!3m4!1s0x54867182dad249f9:0x22b2f0573a197024!8m2!3d49.2879402!4d-123.1130521" target="_blank">
                        Google Maps Direction
                      </a>
                    </iframe>
                  </div>
                </div>
              </Col>
            </Row>
          </article>
        </section>
      </Title>
    )
  }
}
