import React from 'react'
import Title from 'react-document-title'
import { siteName } from 'constants'
import { Link } from 'react-router'

export default () => (
  <Title title={ `${siteName} | Home` }>
    <section className="home-page container grid grid--justify-end">
      <div className="backdrop"></div>
      <article className="promo grid__col-sm-12 grid__col-md-6 grid--align-baseline grid--align-self-center">
        <h3>Prepare To Be The Next</h3>
        <h2 className="text-primary">SINGER ROCKSTAR</h2>
        <Link className="btn btn-lg btn-primary" to="lessons">START YOUR JOURNEY</Link>
      </article>
    </section>
  </Title>
)
