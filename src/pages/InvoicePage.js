import React, { Component } from 'react'
import autobind from 'autobind-decorator'
import Title from 'react-document-title'
import { siteName } from 'constants'
import Cart from '../components/Cart'

@autobind
export default class InvoicePage extends Component {
  print(e) {
    e.preventDefault()

    window.print()
  }

  render() {
    const { actions, store } = this.props
    const userProfile = store.app.get('userProfile')
    const {
      firstName,
      lastName,
      email,
      streetNumber,
      streetName,
      city,
      province,
      country,
      zip,
      shippingMethod,
      comment
    } = userProfile

    return (
      <Title title={ `${siteName} | Invoice` }>
        <div>
          <h2 className="section-header">{siteName} - Invoice</h2>
          <Cart store={store} actions={actions} canRemove={false} editable={false} className="align-center" />
          <a href="#" className="align-center" style={{display: 'block', width: '100%', marginTop: 16}} onClick={this.print}>Print This Page</a>
        </div>
      </Title>
    )
  }
}
