import React, { PropTypes, Component } from 'react'
import autobind from 'autobind-decorator'
import Title from 'react-document-title'
import { find } from 'lodash'
import { siteName } from 'constants'
import { Row, Col } from '../components/Grid'
import Star from '../components/Star'
import Comments from '../components/Comments'
import QuantityCounter from '../components/QuantityCounter'
import Icon from '../components/Icon'

@autobind
export default class LessonDetailPage extends Component {
  static contextTypes = {
    router: PropTypes.object.isRequired
  }

  constructor(props) {
    super(props)

    this.state = {
      currentQuantity: 3
    }
  }

  checkout(e) {
    e.preventDefault()
    this.addToList()
    this.context.router.push('/checkout')
  }

  getLesson() {
    const { params, store } = this.props
    const lessons = store.app.get('lessons')
    const id = parseInt(params.id, 10)

    return find(lessons, { id })
  }

  addToList() {
    const { actions } = this.props
    let lesson = this.getLesson()
    lesson.quantity = this.state.currentQuantity
    actions.addToCart(this.getLesson())
  }

  onSelectQuantity(e) {
    const val = parseInt(e.target.value, 10)
    this.setState({ currentQuantity: val })
  }

  render() {
    const { title, img, desc, price, whosFor, comments } = this.getLesson()
    const { currentQuantity } = this.state
    const whosForList = whosFor.split("\n").map((point, i) => (
      <li key={i}>{point}</li>
    ))

    const rating = comments.reduce((prev, curr) => {
      prev += curr.rating
      return prev
    }, 0) / comments.length

    return (
      <Title title={ `${siteName} | Lesson Detail` }>
        <article className="container lesson-detail">
          <h2 className="section-header">{title} Lesson</h2>

          <Row className="grid--justify-space-between">
            <Col collapseOn="sm" count="6" className="lesson-image">
              <img src={img} alt={title} width="100%" style={{marginTop: -6}}/>
            </Col>
            <Col collapseOn="sm" count="6">
              <p>
                { desc }
              </p>

              <strong>
                <Star rating={Math.round(rating)} />
                <span style={{color: '#999', fontWeight: 'normal', fontSize: '0.9em'}}>
                  {' '}({rating} - {comments.length} reviews)
                </span>
              </strong>

              <strong>Who It's For:</strong>
              <p>
                <ul>
                  { whosForList }
                </ul>
              </p>

              <strong>Get Started!</strong>
              <p>
                Get started with the <b>{title}</b> course!
              </p>
              <QuantityCounter price={price} count={currentQuantity} onSelectQuantity={this.onSelectQuantity}></QuantityCounter>
              <p className="small">
                *Recommended to start with 3 lessons.
              </p>

              <Row>
                <Col collapseOn="xs" count="8" style={{paddingLeft: 0, paddingRight: 0}}>
                  <button className="btn btn-primary full-width" onClick={this.checkout}>Book Lessons</button>
                </Col>
                <Col collapseOn="xs" count="4" style={{paddingRight: 0}}>
                  <button className="btn full-width" onClick={this.addToList}>Add To List</button>
                </Col>
              </Row>
            </Col>
          </Row>

          <Comments title="Reviews" comments={comments} />
        </article>
      </Title>
    )
  }
}
