import React from 'react'
import Title from 'react-document-title'
import { siteName } from 'constants'

import LessonCard from '../components/LessonCard'
import { Row, Col } from '../components/Grid'

export default ({store}) => (
  <Title title={ `${siteName} | Lessons` }>
    <article className="container">
      <h2 className="section-header">Lessons</h2>

      <Row className="wrap lesson-cards">
        {
          store.app.get('lessons').map(lesson => {
            const { id, title, img, desc, comments, disabled } = lesson
            const rating = comments.reduce((prev, curr) => {
              prev += curr.rating
              return prev
            }, 0) / comments.length

            return (
              <Col collapseOn="xs" count="12" key={id} className="grid__col-sm-6 grid__col-lg-4">
                <LessonCard id={id} disabled={disabled} title={title} img={img} rating={Math.round(rating)} desc={desc} />
              </Col>
            )
          })
        }
      </Row>
    </article>
  </Title>
)
