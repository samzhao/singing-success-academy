import React from 'react'

export default ({ children, store, actions }) => (
  <section className="lessons-page">
    { React.cloneElement(children, { store, actions }) }
  </section>
)
