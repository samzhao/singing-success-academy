import React, { Component } from 'react'
import Title from 'react-document-title'
import { siteName } from 'constants'
import { Row, Col } from '../components/Grid'
import UpdateStream from '../components/UpdateStream'

let isServer = false

try {
  document
} catch(e) {
  isServer = true
}

export default class NewsPage extends Component {
  componentWillMount() {
    if (isServer) return

    const script = document.createElement('script')

    script.src = "//platform.twitter.com/widgets.js"
    script.async = true
    script.setAttribute('id', 'twitter-script')

    document.body.appendChild(script)
  }

  componentWillUnmount() {
    if (isServer) return

    const script = document.getElementById('twitter-script')
    document.body.removeChild(script)
  }

  render() {
    return (
      <Title title={ `${siteName} | News & Events` }>
        <section className="news-page">
          <article className="container">
            <h2 className="section-header">News & Events</h2>

            <Row>
              <Col collapseOn="sm" count="8">
                <UpdateStream />
              </Col>

              <Col collapseOn="sm" count="4">
                <strong>Our Tweets</strong>
                <div className="tweets">
                  <a className="twitter-timeline" href="https://twitter.com/TwitterMusic/timelines/393773266801659904">Music Superstars - Curated tweets by TwitterMusic</a>
                </div>
              </Col>
            </Row>
          </article>
        </section>
      </Title>
    )
  }
}
