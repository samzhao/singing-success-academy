import React, { Component } from 'react'
import Title from 'react-document-title'
import { Link } from 'react-router'
import { siteName } from 'constants'
import autobind from 'autobind-decorator'

import { Row, Col } from '../components/Grid'
import Cart from '../components/Cart'
import PaymentForm from '../components/PaymentForm'

@autobind
export default class PaymentPage extends Component {
  confirmOrder() {
    // const { actions } = this.props
    const confirmOrderBtn = document.querySelector('button.confirm-order')
    if (confirmOrderBtn) confirmOrderBtn.click()
  }

  render() {
    const { actions, store } = this.props
    const cartItems = store.app.get('cartItems').toJS()
    const hasCartItems = Boolean(cartItems.length)
    const userProfile = store.app.get('userProfile')

    const noItemsText = (
      <p className="align-center">
        You haven't added any lessons to checkout.<br/>
        <Link to="/lessons">Go find some lessons.</Link>
      </p>
    )

    return (
      <div className="payment-page">
        <h2 className="section-header">Payment</h2>
        <Row>
          <Col count={hasCartItems ? "8" : "12"} collapseOn="sm" align="center">
            { hasCartItems && <PaymentForm userProfile={userProfile} actions={actions} {...this.props} /> }
            { !hasCartItems && noItemsText }
          </Col>
          {
            hasCartItems &&
            <Col count="4" collapseOn="sm" style={{marginBottom: 16}}>
              <Cart store={store} actions={actions} canRemove={false} className="align-center" style={{width: '100%'}} />
              <button onClick={this.confirmOrder} className="btn btn-primary full-width" style={{marginTop: 16}}>Confirm Order</button>
            </Col>
          }
        </Row>
      </div>
    )
  }
}
