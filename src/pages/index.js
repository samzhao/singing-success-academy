export const HomePage = require('./HomePage').default
export const LessonsPage = require('./LessonsPage').default
export const LessonDetailPage = require('./LessonDetailPage').default
export const LessonListingPage = require('./LessonListingPage').default
export const NewsPage = require('./NewsPage').default
export const ContactPage = require('./ContactPage').default
export const AboutPage = require('./AboutPage').default
export const CheckoutPage = require('./CheckoutPage').default
export const CartPage = require('./CartPage').default
export const PaymentPage = require('./PaymentPage').default
export const InvoicePage = require('./InvoicePage').default

export default {
  HomePage,
  LessonsPage,
  LessonDetailPage,
  LessonListingPage,
  NewsPage,
  ContactPage,
  AboutPage,
  CheckoutPage,
  CartPage,
  PaymentPage,
  InvoicePage
}
