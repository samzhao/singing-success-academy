var path = require('path')
var webpack = require('webpack')
var HtmlWebpackPlugin = require('html-webpack-plugin')
var FaviconsWebpackPlugin = require('favicons-webpack-plugin')
var ExtractTextPlugin = require('extract-text-webpack-plugin')

var config = {
  entry: ['./src/app.js'],

  output: {
    publicPath: '/',
    path: './build',
    filename: 'app.js'
  },

  resolve: {
    modulesDirectories: ['node_modules', './src'],
    alias: {
      components: './src/components',
      pages: './src/pages',
      styles: './src/styles',
      assets: './src/assets'
    }
  },

  devServer: {
    stats: 'errors-only',
    historyApiFallback: true,
    hot: true,
    inline: true
  },

  module: {
    loaders: [{
      test: /\.js$/,
      exclude: /node_modules/,
      loader: 'babel',
      query: {
        presets: ['es2015', 'react', 'stage-0'],
        plugins: ['transform-decorators-legacy', 'transform-react-display-name']
      }
    }, {
      test: /\.css$/,
      loader: ExtractTextPlugin.extract('style', 'css')
    }, {
      test: /\.scss$/,
      loader: ExtractTextPlugin.extract('style', 'css!sass')
    }, {
      test: /\.png$/,
      loader: 'url?limit=65536&mimetype=image/png&name=images/[name]_[hash].[ext]'
    }, {
      test: /\.jpg$/,
      loader: 'url?limit=65536&mimetype=image/jpeg&name=images/[name]_[hash].[ext]'
    }, {
      test: /\.woff/,
      loader: 'url?limit=10000&mimetype=application/font-woff&name=fonts/[name]_[hash].[ext]'
    }, {
      test: /\.(ttf|eot|svg)(\?v=.+)?$/,
      loader: 'file?name=fonts/[name]_[hash].[ext]'
    }]
  },

  plugins: [
    new FaviconsWebpackPlugin({
      logo: './src/assets/favicon.png',
      prefix: 'icons/',
      emitStats: false,
      inject: true,
      background: '#fff',
      persistentCache: false,
      icons: {
        android: false,
        appleIcon: false,
        appleStartup: false,
        coast: false,
        favicons: true,
        firefox: false,
        opengraph: false,
        twitter: false,
        yandex: false,
        windows: false
      }
    }),

    new ExtractTextPlugin('style.css'),

    // Generates HTML that includes compiled styles or scripts
    new HtmlWebpackPlugin({
      title: 'Singing Success Academy',
      template: './src/assets/index.html'
    }),

    new webpack.HotModuleReplacementPlugin(),

    new webpack.DefinePlugin({
      'process.env.BROWSER': true
    })
  ]
}

module.exports = config
