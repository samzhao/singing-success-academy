var webpack = require('webpack')
var webpackConfig = require('./webpack.config')

var config = Object.assign(webpackConfig, {
  plugins: (function() {
    webpackConfig.plugins.push(
      new webpack.optimize.UglifyJsPlugin({
        compressor: {
          warnings: false
        }
      }),
      new webpack.DefinePlugin({
        'process.env.NODE_ENV': JSON.stringify('production')
      }),
      new webpack.optimize.DedupePlugin()
    )

    // remove HotModuleReplacementPlugin
    webpackConfig.plugins = webpackConfig.plugins.filter(function(plugin) {
      return !(plugin instanceof webpack.HotModuleReplacementPlugin)
    })

    return webpackConfig.plugins
  })()
})

module.exports = config
