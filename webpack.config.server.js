var fs = require('fs')
var path = require('path')
var webpack = require('webpack')
var clientConfig = require('./webpack.config')

var nodeModules = {}
fs.readdirSync('node_modules')
  .filter(function(x) {
    return ['.bin'].indexOf(x) === -1
  })
  .forEach(function(mod) {
    nodeModules[mod] = 'commonjs ' + mod
  })

var config = {
  entry: './server/index.js',
  target: 'node',
  output: {
    path: path.join(__dirname, 'server/'),
    filename: 'server.js'
  },
  externals: nodeModules,
  module: {
    loaders: clientConfig.module.loaders
  },
  plugins: [
    new webpack.DefinePlugin({
      'process.env.NODE_ENV': JSON.stringify('production'),
      'process.env.BROWSER': false
    }),
    new webpack.optimize.UglifyJsPlugin({
      compressor: {
        warnings: false
      }
    })
  ]
}

module.exports = config
